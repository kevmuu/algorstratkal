var algo_adaptive_kalman 				= {}
algo_adaptive_kalman.engine 			= {}

algo_adaptive_kalman.info_dic 			= {
	id:'algo_adaptive_kalman',
	name:'algo-adaptive-kalman',
	description:`Use a Neural network aided Kalman filter to exploit mean reverting opportunities in a basket of stock`,
	available_bool:false,
	nb_coin_max:10,
	nb_coin_min:2,	
}
algo_adaptive_kalman.kalmanFilter = kalmanFilter_factory()

algo_adaptive_kalman.compute_returns = function(price_dic){
	
	/*
		price_dic={
			list_date:[...]
			dic_coin:{
				algorand:[...],
				...
			}
		}
		compute the historical return for all coins
	*/
	
	var dic_coin			= price_dic.dic_coin || {}
	var list_date 			= price_dic.list_date	|| []
	var dic_coin_return		= {} 
	
	for( let coin_name of Object.keys(dic_coin)){
		
		let list_price 	= dic_coin[coin_name]
		let list_return = []
		for(let i = 1; i<list_price.length; i++){
			price_i 	= list_price[i] 
			price_i_1 	= list_price[i-1]
			return_i 	= (price_i - price_i_1 ) / price_i_1
			list_return.push(return_i)
		}
		dic_coin_return[coin_name] = list_return
	}
	
	
	
	return dic_coin_return
}

algo_adaptive_kalman.sort_returns_array = function(dic_coin_return){
	// return array
	/*
		The function return a dic where containing a list 
		of top performers ( list_top_performers )
		and a list of worst performers (list_worst_performer )
		
		- thereis a window length to computethe average return. 
		- both list are list of tuples [coin_name, avg_return].
		
	*/
	var window_length 					= 10
	var sortable__window_return_avg 	= []
	for( let coin_name of Object.keys(dic_coin_return)){

		if(window_length>dic_coin_return[coin_name].length){
			window_length 		= dic_coin_return[coin_name].length
		}

		let list_return_window 	= dic_coin_return[coin_name].slice(-window_length)
		let last_return 		= list_return_window[list_return_window.length-1]
		// filtered return
		let avg_return			= algo_adaptive_kalman.kalmanFilter(last_return,list_return_window)
		

		sortable__window_return_avg.push([coin_name, avg_return])
	}

	// ranked list_stock by return 
	
	var nb_stocks 			= 2
	sortable__window_return_avg.sort(function(a,b){return b[1] - a[1] })
	
	list_top_performer		= sortable__window_return_avg.slice(0,nb_stocks)
	list_worst_performer 	= sortable__window_return_avg.slice(-nb_stocks)
	return {list_top_performer:list_top_performer,list_worst_performer:list_worst_performer}
}

algo_adaptive_kalman.execute = function(dic_in){
	/*
		1-check if there are stock to sell
			if yes sell stock
			
			
		2-compute current value strategy portfolio
			- if value portfolio is 70% total value 
				- don't buy anything
		3- take 20% of available cash
			divide it on the 3 worst performer equally 
	*/

	var list_top_performer 	= dic_in.list_top_performer
	var list_worst_performer= dic_in.list_worst_performer
	var dic_portfolio 		= dic_in.dic_portfolio
	var portfolio_cash 	= dic_in.portfolio_cash
	
	var list_portfolio_token 	= Object.keys(dic_portfolio)
	for(let token of list_portfolio_token){
		
		if(list_top_performer.indexOf(token)==-1){continue}
		//dic_portfolio = trade_sell(dic_portfolio,token,dic_portfolio[token].qty)
		dic_portfolio = algo_adaptive_kalman.trade_sell_empty(dic_portfolio,token)

	}
	
	list_portfolio_token 	= Object.keys(dic_portfolio)
	var portfolio_value 		= 0
	for(let token of list_portfolio_token){
		let token_price 	= algo_adaptive_kalman.get_token_last_history_price(token)
		portfolio_value 	+=token_price*dic_portfolio[token]
	}
	
	var ratio_cash 	= 100*portfolio_cash / (portfolio_value + portfolio_cash)
	if(ratio_cash < 30 ){
		return
	}
	
	var cash_to_use 		= 0.2 *portfolio_cash
	var list_token_to_buy	= list_worst_performer.filter(function(token){ 
									return list_portfolio_token.indexOf(token)==-1
								})
	var cash_by_token 		= parseInt(cash_to_use /list_token_to_buy.length)
	
	
	for(let token of list_token_to_buy){
		dic_portfolio = algo_adaptive_kalman.trade_buy(dic_portfolio,token,cash_by_token,'cash')
	}
	
	
	return dic_portfolio
}

algo_adaptive_kalman.trade_buy = function(dic_portfolio,token_name,cash,cash_or_qty){
	algo_adaptive_kalman.engine.simulation.execute_transaction({buy_sell:'buy',amount:cash,token_name:token_name,
		account_name:'algo_adaptive_kalman'})
	var dic_portfolio = algo_adaptive_kalman.engine.account.get_account_portfolio('algo_adaptive_kalman')
	return dic_portfolio
}
algo_adaptive_kalman.trade_sell_empty 	= function(dic_portfolio,token_name){
	
	algo_adaptive_kalman.engine.simulation.execute_transaction({buy_sell:'sell_empty',amount:1,token_name:token_name,
		account_name:'algo_adaptive_kalman'})
	var dic_portfolio = algo_adaptive_kalman.engine.account.get_account_portfolio('algo_adaptive_kalman')
	return dic_portfolio
	
}

algo_adaptive_kalman.get_token_last_history_price 	= function(token_name){

	var list_price_history = algo_adaptive_kalman.engine.simulation.get_price_history(token_name)
	if(list_price_history.length==0){
		var last_history_price 	= 0
	}else{
		var last_history_price 	= list_price_history[list_price_history.length-1][1]
	}

	return last_history_price
	
}

algo_adaptive_kalman.strategy_wrapper 	= function(){
	/*
	*
	*/

	// react on day change.
	
	// build dic token price 
	var list_date 	= algo_adaptive_kalman.engine.simulation.list_date_history()
	var list_token 	= algo_adaptive_kalman.engine.simulation.get_list_token()
	
	if(list_date.length<2){
		console.log('not enough data')
		return
	}
	var dic_coin 	= {}
	for(let token_name of list_token){
		var list_price_history = algo_adaptive_kalman.engine.simulation.get_price_history(token_name)
		dic_coin[token_name] = list_price_history.map(function(item){return item[1]})
	}
	
	var price_dic 	= {
		list_date:list_date,
		dic_coin:dic_coin,
	}
	
	
	// 
	var dic_coin_return 		= algo_adaptive_kalman.compute_returns(price_dic)
	var dic_sorted_performer 	= algo_adaptive_kalman.sort_returns_array(dic_coin_return)
	
	var dic_portfolio 			= algo_adaptive_kalman.engine.account.get_account_portfolio('algo_adaptive_kalman')
	var portfolio_cash 			= algo_adaptive_kalman.engine.account.get_account_cash('algo_adaptive_kalman')
	algo_adaptive_kalman.execute({
		list_top_performer 		: dic_sorted_performer.list_top_performer.map(function(item){return item[0]}),
		list_worst_performer 	: dic_sorted_performer.list_worst_performer.map(function(item){return item[0]}),
		dic_portfolio 			: dic_portfolio,
		portfolio_cash			: portfolio_cash,
	})
	// export metrics
	
}
algo_adaptive_kalman.algo_init  = function(engine){
	
	algo_adaptive_kalman.engine = engine
	// register account
	algo_adaptive_kalman.engine.account.register_account({account_name:'algo_adaptive_kalman'})

	// register algo to exchange
	algo_adaptive_kalman.engine.simulation.register_step_callback({'callback_name':'algo_adaptive_kalman.strategy_wrapper','callback_func':algo_adaptive_kalman.strategy_wrapper})


}