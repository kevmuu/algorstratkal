var get_id = function(){
	return Math.floor(Math.random() * 1000*1000)
}

function Neuron(bias) {
	this.id = get_id();

	this.bias = bias == undefined ? Math.random() * 2 - 1 : bias;
	this.squash;
	this.cost;
  
	this.incoming = {
		targets: {},
		weights: {} 
	}
	this.outgoing = {
		targets: {}, 
		weights: {} 
	}
  
	this._output; // f'(x)
	this.output; // f(x)
	this.error; // E'(f(x))
	this._error;// E(f(x))
  
	this.connect = function(neuron, weight) {
		this.outgoing.targets[neuron.id] = neuron;
		neuron.incoming.targets[this.id] = this;
		this.outgoing.weights[neuron.id] = neuron.incoming.weights[this.id] = weight == undefined ? Math.random() * 2 - 1 : weight;
	}
  
	this.activate = function(input) {
		
		var self = this;

		function sigmactivfunc(x){ 		return 1 / (1 + Math.exp(-x)) 						} // f(x)
		function _sigmactivfunc(x){ 	return sigmactivfunc(x) * (1 - sigmactivfunc(x)) 	} // f'(x)

		if(input != undefined) {
			this._output = 1; // f'(x)
			this.output = input; // f(x)
		}else{
			var sum = Object.keys(this.incoming.targets).reduce(function(total, target, index) {
				return total += self.incoming.targets[target].output * self.incoming.weights[target];
			}, this.bias);
		  
			this._output = _sigmactivfunc(sum); 
			this.output = sigmactivfunc(sum); 
		}

		return this.output;
	}
  
	this.propagate = function(target, rate=0.3) {
		
		var self = this;
    
		var sum = target == undefined ? Object.keys(this.outgoing.targets).reduce(function(total, target, index) {
			self.outgoing.targets[target].incoming.weights[self.id] = self.outgoing.weights[target] -= rate * self.outgoing.targets[target].error * self.output;
        
			return total += self.outgoing.targets[target].error * self.outgoing.weights[target];
		}, 0) : this.output - target;
    
		
		
		this.error = sum * this._output
		this.bias -= rate * this.error;
    
		return this.error;
  }
}

