


var inputs 		= [new Neuron(), new Neuron(), new Neuron(), new Neuron(), new Neuron(), new Neuron(), new Neuron(),new Neuron(),]; 
var hiddens 	= [new Neuron(), new Neuron(), new Neuron(), new Neuron()]; 
var outputs 	= [new Neuron()]; 

// connect input layer to fidden
for(let index_input = 0 ; index_input<8;	index_input++ ){

	for(let index_hidden = 0 ; index_hidden<4;	index_hidden++ ){
		let _inputs 			= dic_weight.inputs || {}
		let node_outgoing_dic 	= (_inputs[index_input] || {}).outgoing || {}
		
		let weight 				= node_outgoing_dic[index_hidden].weight // or undefined
		inputs[index_input].connect(hiddens[index_hidden],weight);
		console.log(index_input,index_hidden)
		
	}
}


// Connect Hidden Layer to Output Layer
for(let index_hidden = 0 ; index_hidden<4;	index_hidden++ ){
	let _hiddens 			= dic_weight.hiddens || {}
	let node_outgoing_dic 	= (_hiddens[index_hidden] || {}).outgoing || {}

	let weight 				= node_outgoing_dic[0] // or undefined
	
	hiddens[index_hidden].connect(outputs[0],weight);
}
	


var activate = (input) => {
	inputs.forEach((neuron, i) => neuron.activate(input[i]));
	hiddens.forEach(neuron => neuron.activate());
	return outputs.map(neuron => neuron.activate());
};
var propagate = (target) => {
  outputs.forEach((neuron, t) => neuron.propagate(target[t]));
  hiddens.forEach(neuron => neuron.propagate());
  return inputs.forEach(neuron => neuron.propagate());
};


var neural_network = {
	predict:function(vector){
		return activate(vector)
	}
} 
