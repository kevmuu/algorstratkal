//https://www.wouterbulten.nl/posts/kalman-filters-explained-removing-noise-from-rssi-signals/

var KalmaFilter_factory 	= function(R,Q){
	
	var _proto 	= {


		filter:function(z,u){
			
			if(isNaN(this.x)){
				this.x 		= (1 / this.C) * z;
				this.cov 	= (1 / this.C) * this.Q * (1 / this.C);
			}else{
				
				const predX = this.predict(u);
				const predCov = this.uncertainty();
				
				const K = predCov*this.C*(1 / ((this.C * predCov * this.C) + this.Q));

				this.x 		= predX + K * (z - (this.C * predX));
				this.cov 	= predCov - (K * this.C * predCov);
				
			}
			
			return this.x;
		},
		predict:function(vector){
			// using neural network with previous estimatin

			const xNeural 	= this.neural_network.predict(vector)
			return (this.A * xNeural);
		},
		uncertainty:function() {
			return ((this.A * this.cov) * this.A) + this.R;
		},
		lastMeasurement:function(){
			// get the last price provided by market api
			return this.x;
		},
		setMeasurementNoise: function(noise) {
			this.Q = noise;
		},
		setProcessNoise: function(noise) {
			this.R = noise;
		},
		init:function(R,Q){
			
			this.R  = R // 1
			this.Q 	= Q // 1
			
			this.A 	= 1
			this.B 	= 0
			this.C 	= 1
			
			this.cov 	= NaN;
			this.x 		= NaN; 
			
			this.neural_network = neural_network
		}
	} 
	
	var _obj 	= Object.create(_proto)
	_obj.init(R,Q)
	return _obj
	
}